Date.prototype.MaxDate=function(time){
	let sTime=this;
	let eTime=time;
	typeof eTime==='string'&&(eTime=new Date(time));
	sTime=sTime.getTime();
	eTime=eTime.getTime();
	return sTime>eTime;
}
Date.prototype.MinDate=function(time){
	let sTime=this;
	let eTime=time;
	typeof eTime==='string'&&(eTime=new Date(time));
	sTime=sTime.getTime();
	eTime=eTime.getTime();
	return sTime<eTime;
}
Date.prototype.EqualDate=function(time){
	let sTime=this;
	let eTime=time;
	typeof eTime==='string'&&(eTime=new Date(time));
	sTime=sTime.getTime();
	eTime=eTime.getTime();
	return sTime<eTime;
}
Date.prototype.format = function(fmt){
	var o = {
		"M+" : this.getMonth()+1,                 //月份
		"d+" : this.getDate(),                    //日
		"h+" : this.getHours(),                   //小时
		"m+" : this.getMinutes(),                 //分
		"s+" : this.getSeconds(),                 //秒
		"q+" : Math.floor((this.getMonth()+3)/3), //季度
		"S"  : this.getMilliseconds()             //毫秒
	};
	if(/(y+)/.test(fmt))
		fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
	for(var k in o)
		if(new RegExp("("+ k +")").test(fmt))
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
	return fmt;
}

Date.prototype.addDay=function(time){
	return new Date(this.getTime()+86400000*time);
}
Date.prototype.addHours=function(time){
	return new Date(this.getTime()+3600000*time);
}
Date.prototype.addMinutes=function(time){
	return new Date(this.getTime()+60000*time);
}
Date.prototype.addSeconds=function(time){
	return new Date(this.getTime()+1000*time);
}
Date.prototype.addMonth=function(time){
	let date=this;
	let monthnum = parseInt(time);
	let year = date.getFullYear();
	let month =date.getMonth()+1;
	let day =date.getDate();
	let newyear = year;
	let newmonth = month + monthnum;
	let newday = day;
	if (month + monthnum > 12)
	{
		newyear = year + 1;
		newmonth = month + monthnum - 12;
		newday = day;
	}
	console.log(day);
	let addTime=date.getTime()-new Date(year + "-" + month + "-" + day).getTime();
	let newdate = new Date(new Date(newyear + "-" + newmonth + "-" + newday).getTime()+addTime);
	return newdate;
}
Date.prototype.addYear=function(time){
	this.setFullYear(this.getFullYear()+1)
	return this;
}
